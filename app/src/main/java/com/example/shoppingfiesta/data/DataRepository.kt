package com.example.shoppingfiesta.data

import com.example.shoppingfiesta.framework.retrofit.TrendingMoviesResponse
import io.reactivex.Single

class DataRepository(private val onlineDataSource: OnlineDataSource) {

    fun downloadTrendingMovies(): Single<TrendingMoviesResponse> {
        return onlineDataSource.getTrendingMovies()
    }

    interface OnlineDataSource {
        fun getTrendingMovies(): Single<TrendingMoviesResponse>
    }
}