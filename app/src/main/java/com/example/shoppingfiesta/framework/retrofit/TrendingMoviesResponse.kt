package com.example.shoppingfiesta.framework.retrofit

import com.example.shoppingfiesta.domain.PurchaseItemModel
import com.google.gson.annotations.SerializedName

data class TrendingMoviesResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val itemList: ArrayList<PurchaseItemModel>
)