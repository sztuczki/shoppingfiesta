package com.example.shoppingfiesta.framework.retrofit

import com.example.shoppingfiesta.data.DataRepository
import io.reactivex.Single
import retrofit2.Retrofit

//this value should be stored in keyword.properties and excluded from the online repository
const val TMDB_API_KEY = "7d9529ea8ccf8d82e2cc8114d4b8a09b"

class RetrofitDataStore(private val client: Retrofit) : DataRepository.OnlineDataSource {

    override fun getTrendingMovies(): Single<TrendingMoviesResponse> {
        val service = client.create(TMDbService::class.java)
        return service.getTrendingMovies(TMDB_API_KEY)
    }
}