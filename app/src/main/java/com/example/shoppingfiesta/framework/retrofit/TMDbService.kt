package com.example.shoppingfiesta.framework.retrofit

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface TMDbService {

    @GET("trending/movie/week")
    fun getTrendingMovies(@Query("api_key") apiKey: String): Single<TrendingMoviesResponse>
}