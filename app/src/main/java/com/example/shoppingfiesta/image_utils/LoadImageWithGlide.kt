package com.example.shoppingfiesta.image_utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners

class LoadImageWithGlide {
    operator fun invoke(view: ImageView, imageUrl: String) {
        Glide.with(view.context)
            .load(imageUrl)
            .transform(CenterCrop(), RoundedCorners(32))
            .into(view)
    }
}