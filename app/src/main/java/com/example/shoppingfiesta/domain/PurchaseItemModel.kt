package com.example.shoppingfiesta.domain

import com.google.gson.annotations.SerializedName

data class PurchaseItemModel(
    @SerializedName("poster_path")
    val imagePath: String?,
    @SerializedName("original_title")
    val label: String,
    @SerializedName("vote_average")
    val rating: Double,
    @SerializedName("vote_count")
    val voteCount: Int
)