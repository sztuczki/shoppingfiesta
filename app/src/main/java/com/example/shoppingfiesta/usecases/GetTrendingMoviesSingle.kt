package com.example.shoppingfiesta.usecases

import com.example.shoppingfiesta.data.DataRepository
import com.example.shoppingfiesta.framework.retrofit.TrendingMoviesResponse
import io.reactivex.Single

class GetTrendingMoviesSingle(private val onlineDataSource: DataRepository) {

    operator fun invoke(): Single<TrendingMoviesResponse> {
        return onlineDataSource.downloadTrendingMovies()
    }
}