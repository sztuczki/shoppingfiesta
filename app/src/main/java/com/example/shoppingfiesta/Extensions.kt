package com.example.shoppingfiesta

import com.example.shoppingfiesta.domain.PurchaseItemModel
import java.text.DecimalFormat

fun Double.toPriceString(): String {
    val format = DecimalFormat("#.00")
    val formattedString = format.format(this)
    return "$formattedString zł"
}

fun Int.toQuantityString(): String {
    val formattedString = String.format(this.toString())
    return "$formattedString szt."
}

fun String.toImageUrl(): String {
    return "https://image.tmdb.org/t/p/w200/$this"
}

fun ArrayList<PurchaseItemModel>.getPriceSummaryString(): String {
    var priceSummary = 0.0
    for (item in this) {
        priceSummary += item.rating
    }
    return priceSummary.toPriceString()
}