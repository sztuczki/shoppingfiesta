package com.example.shoppingfiesta.ui.features.purchase_confirmation

import com.example.shoppingfiesta.domain.PurchaseItemModel

interface PurchaseConfirmationContract {

    interface View {
        fun updateForResult(result: ArrayList<PurchaseItemModel>)
    }

    interface Presenter {
        fun getTrendingMovies()
        fun onStop()
        fun onDestroy()
    }
}