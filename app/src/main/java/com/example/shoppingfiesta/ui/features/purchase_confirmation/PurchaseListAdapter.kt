package com.example.shoppingfiesta.ui.features.purchase_confirmation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppingfiesta.R
import com.example.shoppingfiesta.domain.PurchaseItemModel
import com.example.shoppingfiesta.image_utils.LoadImageWithGlide
import com.example.shoppingfiesta.toImageUrl
import com.example.shoppingfiesta.toPriceString
import com.example.shoppingfiesta.toQuantityString

class PurchaseListAdapter(val loadImageWithGlide: LoadImageWithGlide) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var purchaseItemList = ArrayList<PurchaseItemModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_purchase_list, parent, false)
        return PurchaseItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return purchaseItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PurchaseItemViewHolder).bindData(purchaseItemList[position])
    }

    inner class PurchaseItemViewHolder(val item: View) : RecyclerView.ViewHolder(item) {

        private val urlTextView = item.findViewById<ImageView>(R.id.image_item_url)
        private val labelTextView = item.findViewById<TextView>(R.id.text_item_label)
        private val voteCountTextView = item.findViewById<TextView>(R.id.text_item_vote_count)
        private val scoreTextView = item.findViewById<TextView>(R.id.text_item_score)


        fun bindData(itemModel: PurchaseItemModel) {
            itemModel.imagePath?.let {
                loadImageWithGlide.invoke(urlTextView, it.toImageUrl())
            }
            labelTextView.text = itemModel.label
            voteCountTextView.text = itemModel.voteCount.toQuantityString()
            scoreTextView.text = itemModel.rating.toPriceString()
        }
    }

    fun updateForResult(list: ArrayList<PurchaseItemModel>) {
        this.purchaseItemList = list
        notifyDataSetChanged()
    }
}