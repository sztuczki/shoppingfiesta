package com.example.shoppingfiesta.ui.features.purchase_confirmation

import com.example.shoppingfiesta.domain.PurchaseItemModel
import com.example.shoppingfiesta.usecases.GetTrendingMoviesSingle
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class PurchaseConfirmationPresenter(
    private var view: PurchaseConfirmationContract.View?,
    private val getTrendingMoviesSingle: GetTrendingMoviesSingle,
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
) : PurchaseConfirmationContract.Presenter {

    override fun getTrendingMovies() {
        val trendingMoviesDisposable = getTrendingMoviesSingle()
            .map { result ->
                result.itemList
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<ArrayList<PurchaseItemModel>>() {
                override fun onSuccess(t: ArrayList<PurchaseItemModel>) {
                    view?.updateForResult(t)
                }

                override fun onError(e: Throwable) {
                }
            })
        compositeDisposable.add(trendingMoviesDisposable)
    }

    override fun onStop() {
        compositeDisposable.clear()
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        view = null
    }
}