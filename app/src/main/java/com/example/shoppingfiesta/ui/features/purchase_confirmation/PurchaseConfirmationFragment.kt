package com.example.shoppingfiesta.ui.features.purchase_confirmation


import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shoppingfiesta.R
import com.example.shoppingfiesta.data.DataRepository
import com.example.shoppingfiesta.domain.PurchaseItemModel
import com.example.shoppingfiesta.framework.retrofit.RetrofitClient
import com.example.shoppingfiesta.framework.retrofit.RetrofitDataStore
import com.example.shoppingfiesta.getPriceSummaryString
import com.example.shoppingfiesta.image_utils.LoadImageWithGlide
import com.example.shoppingfiesta.usecases.GetTrendingMoviesSingle
import kotlinx.android.synthetic.main.fragment_purchase_confirmation.*

class PurchaseConfirmationFragment : Fragment(), PurchaseConfirmationContract.View {

    lateinit var listItemAdapter: PurchaseListAdapter
    lateinit var presenter: PurchaseConfirmationContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val client = RetrofitClient().getClient()
        val retrofitDataStore = RetrofitDataStore(client)
        val repository = DataRepository(retrofitDataStore)
        val getTrendingMoviesSingle = GetTrendingMoviesSingle(repository)
        presenter = PurchaseConfirmationPresenter(this, getTrendingMoviesSingle)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_purchase_confirmation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv_purchase_list.apply {
            adapter = PurchaseListAdapter(LoadImageWithGlide()).also { listItemAdapter = it }
            layoutManager = LinearLayoutManager(view.context)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.getTrendingMovies()
    }

    override fun updateForResult(result: ArrayList<PurchaseItemModel>) {
        listItemAdapter.updateForResult(result)
        val purchaseConfirmationMessage =
            String.format(getString(R.string.purchase_confirmation_message), result.getPriceSummaryString())
        text_purchase_confirmation?.text = purchaseConfirmationMessage
        TransitionManager.beginDelayedTransition(root_purchase_confirmation)
        group_purchase_confirmation.visibility = View.VISIBLE
    }

    override fun onStop() {
        presenter.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}